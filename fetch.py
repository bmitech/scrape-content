import time

import sys
from requests import post
from urllib.request import urlretrieve
from os.path import join, splitext, curdir, abspath, exists

""" Default Variables """
base_url = "http://player.canneslions.com/api/player"

headers = {'Content-type': 'application/json'}

cannes_lion = [
    "creative effectiveness", "cyber", "design", "digital craft", "direct", "film",
    "film craft", "glass lion", "grand prix for good", "integrated",
    "mobile", "outdoor", "pr", "print and publishing", "product design", "promo and activation", "radio", "titanium"
]

lions_innovation = [
    "creative data", "innovation"
]

lions_health = [
    "health and wellness", "pharma"
]

lions_entertainment = [
    "entertainment", "Entertainment  for Music"
]

raw_body = '{{"ResultsPerPage":99999,"PageIndex":1,"IsDetailedSearch":1,"Awards":["Grand Prix","Gold","Silver","Bronze","Shortlist","No Award"],"Lion":"{}","Section":"","SubSection":""}}'

media_count = 0


def reporthook(count, block_size, total_size):
    global start_time
    if count == 0:
        start_time = time.time()
        return
    duration = time.time() - start_time
    progress_size = int(count * block_size)
    speed = int(progress_size / (1024 * duration))
    percent = int(count * block_size * 100 / total_size)
    sys.stdout.write("\r...%d%%, %d MB, %d KB/s, %d seconds passed" %
                    (percent, progress_size / (1024 * 1024), speed, duration))
    sys.stdout.flush()


for section in [cannes_lion, lions_innovation, lions_health, lions_entertainment]:
    for sub_section in section:
        section_media_count = 0
        print('[START] Downloading ' + sub_section + ' section.')
        hit_url = post(base_url, headers=headers, data=raw_body.format(sub_section))
        for res in hit_url.json()['Results']:
            file_title = "[" + str(res["Section"]) + " - " + res["Prizes"][0]["Description"] + "] " + res["Title"].replace("/", "-")
            if "Strategy" in res["EntryMemos"]:
                entry_memos = res["EntryMemos"]["Strategy"]
            else:
                entry_memos = ''
            file_metadata = "Title: " + str(res["Title"]) + "\n" + "Client: " + str(
                res["Client"]) + "\n" + "Agencies: " + str(res["Agencies"]) + "\n" + "Section: " + str(
                res["Section"]) + "\n" + "Media Description: " + str(
                res["MediaDescription"]) + "\n" + "Category: " + str(res["Category"]) + "\n" + "Festival Name: " + str(
                res["FestivalName"]) + "\n" + "Festival Year: " + str(res["FestivalYear"]) + "\n" + "Entry Memos: " + entry_memos + "\n" + "Entrant Company: \n" + "Company Name: " + str(
                res["EntrantCompany"]["CompanyName"]) + "\n" + "Co Town: " + str(
                res["EntrantCompany"]["CoTown"]) + "\n" + "Countries: " + str(
                res["EntrantCompany"]["Countries"][0]) + "\n" + "Product: " + str(res["Product"])
            if "OtherMedia" in res:
                if len(res["OtherMedia"]) > 0:
                    for media in res["OtherMedia"]:
                        f, fext = splitext(media["MediaUri"].split("?")[0])
                        file_name = "".join([file_title, fext])
                        if exists(file_name):
                            print("Skipping " + file_name)
                        else:
                            print("Starting: " + file_name)
                            with open(file_title + '.txt', 'w') as media_metadata:
                                media_metadata.write(file_metadata)
                            media_metadata.closed
                            urlretrieve(media["MediaUri"], file_name, reporthook)
                            print(file_name + ' download completed!')
                        media_count += 1
                        section_media_count += 1
        else:
            print("Media in this section: " + str(section_media_count))
    else:
        print(sub_section + ' download completed!')
else:
    print("Total Media Count: " + str(media_count))
