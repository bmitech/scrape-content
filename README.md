# Media Scrape v1.0 #

The purpose of this repository is web scraping.

### What is this repository for? ###

* Scraping is done exclusively dependent on API.
* API are called with raw JSON param using POST method
* Response is converted to json object
* Traversing is done based on the media availability.
* Media (here *.mp4) is downloaded, with metadata (which gets stored with same file name in *.txt format)
* Basic check of file name exists, that means it won't download the same file again.
* Official [Cannes](https://www.canneslions.com/the-cannes-lions-archive) website is used to scrape videos.

### How do I get set up? ###

* Install Python 3 (it's must else won't work!)
* `pip install requests`
* *Nix System will be better

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Foe any query contact Gaurav Dave (gauravdave@brandmovers.in)